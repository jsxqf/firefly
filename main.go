package main

import (
	_ "firefly.com/firefly/routers"
	"github.com/astaxie/beego"
	"firefly.com/firefly/controllers"
	"net/http"
	"html/template"
)

func main() {
	beego.SetLogger("file", `{"filename":"logs/firefly.log"}`)
	beego.ErrorController(&controllers.ErrorController{})
//	beego.Errorhandler("404", &controllers.ErrorController{}.Error404())
	beego.Errorhandler("404", error404)
	beego.Run()
}

func error404(rw http.ResponseWriter, r *http.Request){

	t,_:= template.New("404.html").ParseFiles(beego.ViewsPath+"/404.html")
	data :=make(map[string]interface{})
	data["content"] = "page not found"
	t.Execute(rw, data)
}