package member
import (
	"github.com/astaxie/beego"
	"crypto/sha256"
	"io"
	"gopkg.in/mgo.v2/bson"
	"firefly.com/firefly/models/common"
)

func DoLogin(email string, password string) (ok bool, ret common.Member) {

	ok = false
	ret = common.Member{}
	session := common.Session.Copy()
	c := session.DB("firefly").C("member")
	password = addSalt(email, password)
	err := c.Find(bson.M{"email": email, "password": password}).One(&ret)
	if err != nil {
		beego.Error(common.DBQueryError)
	}else {
		// compare empty struct
		if ret !=(common.Member{}) {
			ok = true
		}
	}

	return
}

func DoSignUp(member *common.Member) (ok bool) {

	ok = false
	if validate(member) && !checkExist(member) {

		session := common.Session.Copy()
		c := session.DB("firefly").C("member")
		// password salt
		member.Password = addSalt(member.Email, member.Password)
		// pass by ref
		err := c.Insert(member)
		if err != nil {
			beego.Error(common.DBInsertError)
		}else {
			ok = true
		}
	}else {
		beego.Error("member validate error")
	}

	return
}
func validate(member *common.Member) (ok bool) {

	ok = false
	if len(member.UserName) >= 5 || len(member.Password) >= 5 {
		ok = true
		return
	}

	return
}
func checkExist(member *common.Member) (ok bool) {

	ok = true
	session := common.Session.Copy()
	c := session.DB("firefly").C("member")
	ret, err := c.Find(bson.M{"email": member.Email}).Count()
	if err != nil {
		beego.Error(common.DBQueryError)
	}else {
		if ret == 0 {
			ok = false
		}
	}

	return
}

func GetFile(owner string) (files []common.FireflyFile) {

	session := common.Session.Copy()
	c := session.DB("firefly").C("file")
	err := c.Find(bson.M{"owner": owner}).Sort("-create_at").All(&files)
	if err != nil {
		beego.Error(common.DBQueryError)
		return
	}
	return

	return
}
func addSalt(email string, password string) (hashedPass string) {

	h := sha256.New()
	io.WriteString(h, email)
	io.WriteString(h, common.JsonConfig.String("SALT_KEY"))
	io.WriteString(h, password)
	hashedPass = string(h.Sum(nil))

	return
}
