package common
import (
	"errors"
	"github.com/astaxie/beego/config"
	"gopkg.in/mgo.v2/bson"
	"gopkg.in/mgo.v2"
	"github.com/astaxie/beego"
)

// common func
type Message struct {
	Ok      bool `ok`
	Message string `message`
	Data    string `data`
	Extra   string `extra`
}

type Position struct {
	Ok        bool `json:"ok"`
	Type      string `json:"type"`
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
}

type FireflyFile struct {
	Id             bson.ObjectId `_id`
	Owner          string `owner`
	FileName       string `file_name`
	Position       string `position`
	Expire         int64 `expire`
	// valid;todo_removed;
	// session id
	BoxViewSession string `box_view_session`
	CreateAt       int64 `create_at`
	UpdateAt       int64 `update_at`
}

/*
member start
*/
type Member struct {
	UserName string `username`
	Email    string `email`
	Password string `password`
	CreateAt int64 `create_at`
}
/*
member end
*/

/*
box view start
*/
type DocumentRet struct {
	Type     string `type`
	Id       string `id`
	Status   string `status`
	Name     string `name`
	CreateAt string `create_at`
}
type Urls struct {
	View     string `view`
	Assets   string `assets`
	Realtime string `realtime`
}
type SessionRet struct {
	Type      string `type`
	Id        string `id`
	Document  DocumentRet `document`
	ExpiresAt string `expires_at`
	Urls      Urls   `urls`
}
/*
box view end
*/

/*
global value
*/
type Global struct {
	Name  string
	Value int64
}
/*
global end
*/

// error
var (
	DBConnectionError = errors.New("DB: Connection Fail")
	DBQueryError = errors.New("DB: Query Fail")
	DBInsertError = errors.New("DB: Insert Fail")
	DBUpdateError = errors.New("DB: Update Fail")
	JSONConfigError = errors.New("Config: JSON Config Parse Fail")
	FileEncryptionError = errors.New("File: Encryption Fail")
	FileDecryptionError = errors.New("File: Decryption Fail")
	FileOpenError = errors.New("File: Open Fail")
	FileGetFromFormError = errors.New("File: Get File From Form Fail")
	FileSaveToDBError = errors.New("File: Save to DB Fail")

	BoxAPIRequestError = errors.New("Box: API Request Fail")
)


const (
	KeySize = 32
	NonceSize = 24
)

var JsonConfig config.ConfigContainer
// singleton?
var Session *mgo.Session

func init() {

	var err error
	JsonConfig, err = config.NewConfig("json", "./conf/config.json")
	if err != nil {
		beego.Error(JSONConfigError)
	}

	Session, err = mgo.Dial(JsonConfig.String("MG_URL"))
	if err != nil {
		beego.Error(DBConnectionError)
	}
}