package fireflyfile
import (
	"crypto/rand"
	"golang.org/x/crypto/nacl/secretbox"
	"github.com/astaxie/beego"
	"gopkg.in/mgo.v2/bson"
	"time"
	"github.com/astaxie/beego/httplib"
	"encoding/json"
	"firefly.com/firefly/models/common"
	"encoding/hex"
)

var key [common.KeySize]byte
func init() {

	keyInByteSlice := []byte(common.JsonConfig.String("FILE_ENCRYPTION_KEY"))
	copy(key[:], keyInByteSlice[:common.KeySize])
}

func SaveFileToDB(file common.FireflyFile, content []byte) (ok bool) {

	ok = false
	session := common.Session.Copy()

	c := session.DB("firefly").C("file")
	err := c.Insert(&file)
	if err != nil {
		beego.Error(common.DBInsertError)
		return
	}
	ok = true

	return
}
func UpdateBoxView(id bson.ObjectId, boxViewSession string) (ok bool) {

	ok = false
	session := common.Session.Copy()
	c := session.DB("firefly").C("file")
	// yo
	err := c.UpdateId(id, bson.M{"$set": bson.M{"box_view_session": boxViewSession}})
	if err != nil {
		beego.Error(common.DBUpdateError)
		return
	}
	return
}

// encrypted
func SaveFileToDisk() {

	//		encryptedContent := content
	//		encryptedContent, err := encryptFile(key, content)
	//		if err != nil {
	//			beego.Error(common.FileEncryptionError)
	//			return
	//		}else {
	//			f, err := os.OpenFile(common.JsonConfig.String("FILE_FOLDER")+file.Id.Hex(), os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	//			defer f.Close()
	//			if err != nil {
	//
	//				beego.Error(common.FileOpenError)
	//				return
	//			}
	//			io.Copy(f, bytes.NewReader(encryptedContent))
	//			ok = true
	//			return
	//		}
}

func GetFileById(id string) (file common.FireflyFile) {

	session := common.Session.Copy()
	c := session.DB("firefly").C("file")
	// recover - used in defer
	if bson.IsObjectIdHex(id) {
		objectId := bson.ObjectIdHex(id)
		err := c.Find(bson.M{"_id": objectId}).One(&file)
		if err != nil {
			beego.Error(common.DBQueryError)
			return
		}
	}
	return
}

//func GetBoxViewSession(id string) (sessionUrl string) {
//
//	session := common.Session.Copy()
//	c := session.DB("firefly").C("file")
//	if bson.IsObjectIdHex(id) {
//
//		objectId := bson.ObjectIdHex(id)
//		var file common.FireflyFile
//		err := c.Find(bson.M{"_id": objectId}).One(&file)
//		if err != nil {
//			beego.Error(common.DBQueryError)
//			return
//		}else {
//			now := time.Now().Unix()
//			// box view expire in 12 hours
//			if file.BoxViewSession == "" || (now - file.UpdateAt) > 43200 {
//				sessionUrl = RefreshBoxViewSession(id)
//				// update file
//				c.Update(bson.M{"_id": objectId}, bson.M{"boxviewsession": sessionUrl})
//			}
//		}
//	}
//
//	return
//}

//func RefreshBoxViewSession(id string) (boxViewSession string) {
//
//	f, err := os.OpenFile(common.JsonConfig.String("FILE_FOLDER")+common.JsonConfig.String("TMP_FILE_FOLDER")+id, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
//	defer f.Close()
//	if err != nil {
//		beego.Error(common.FileOpenError)
//	}
//	fileContent, _ := ioutil.ReadFile(common.JsonConfig.String("FILE_FOLDER")+id)
//	content, _ := decryptFile(key, fileContent)
//	io.Copy(f, bytes.NewReader(content))
//
//	req := httplib.Post(common.JsonConfig.String("BOX_VIEW_API_URL")+"documents")
//	req.Header("Authorization", "Token "+common.JsonConfig.String("BOX_VIEW_TOKEN"))
//	req.Header("Content-Type", "application/json")
//	// json data
//	req.Body(`{"url": "https://cloud.box.com/shared/static/jedg2r0kr9qy3you6yb0dcdut2z1i4yc.pdf"}`)
//
//
//	var docRet common.DocumentRet
//	err = req.ToJson(&docRet)
//	if err != nil {
//		beego.Error(err)
//	}
//
//	beego.Notice("document:"+docRet.Id)
//
//	req2 := httplib.Post(common.JsonConfig.String("BOX_VIEW_API_URL")+"sessions")
//	req2.Header("Authorization", "Token "+common.JsonConfig.String("BOX_VIEW_TOKEN"))
//	req2.Header("Content-Type", "application/json")
//
//	sb := map[string]string{"document_id": docRet.Id, "duration": "1440"}
//
//	ret, _ := json.Marshal(sb)
//	beego.Notice(string(ret))
//	req2.Body(ret)
//
//	// required
//	time.Sleep(time.Second*2)
//
//	var sesRet common.SessionRet
//	limit := 0
//	for {
//		response, err := req2.Response()
//		if err != nil {
//			beego.Error(err)
//		}
//		if response.StatusCode == 201 {
//			err = req2.ToJson(&sesRet)
//			if err != nil {
//				beego.Error(err)
//			}
//			break
//		}
//		limit++
//		if limit > 10 {
//			break
//		}
//		time.Sleep(time.Second*3)
//	}
//	boxViewSession = sesRet.Urls.View
//
//	beego.Notice("box view session:"+boxViewSession)
//
//	return
//}

func GenerateBoxViewSession(id string) (boxViewSession string) {

	boxViewSession = ""
	docReq := httplib.Post(common.JsonConfig.String("BOX_VIEW_API_URL")+"documents")
	docReq.Header("Authorization", "Token "+common.JsonConfig.String("BOX_VIEW_TOKEN"))
	docReq.Header("Content-Type", "application/json")
	docBody := map[string]string{"url": common.JsonConfig.String("DOMAIN")+"bd/"+id}
	docBodyInByte, _ := json.Marshal(docBody)
	//	docReq.Body(`{"url": "https://cloud.box.com/shared/static/jedg2r0kr9qy3you6yb0dcdut2z1i4yc.pdf"}`)

	docReq.Body(docBodyInByte)

	var docRet common.DocumentRet
	err := docReq.ToJson(&docRet)
	if err != nil {
		return
	}

	beego.Notice("document_id: "+docRet.Id)


	sesReq := httplib.Post(common.JsonConfig.String("BOX_VIEW_API_URL")+"sessions")
	sesReq.Header("Authorization", "Token "+common.JsonConfig.String("BOX_VIEW_TOKEN"))
	sesReq.Header("Content-Type", "application/json")

	sesBody := map[string]string{"document_id": docRet.Id, "duration": "1440"}
	sesBodyInByte, _ := json.Marshal(sesBody)
	sesReq.Body(sesBodyInByte)

	// cool down box api
	time.Sleep(time.Second * 3)

	var sesRet common.SessionRet
	limit := 0
	for {
		beego.Notice("checking session created...")
		response, err := sesReq.Response()
		if err != nil {
			beego.Error(common.BoxAPIRequestError)
			break
		}
		if response.StatusCode == 201 {
			sesReq.ToJson(&sesRet)
			sesInString, _ := json.Marshal(sesRet)
			boxViewSession = string(sesInString)
			beego.Notice("boxviewsession: "+boxViewSession)
			break
		}
		limit++
		if limit > 5 {
			break
		}
		// send request every 5s if failed
		time.Sleep(time.Second * 5)
	}

	return
}

func encryptFile(key [common.KeySize]byte, content []byte) ([]byte, error) {

	nonce, err := generateNonce()
	if err != nil {
		return nil, common.FileEncryptionError
	}
	out := make([]byte, len(nonce))
	copy(out, nonce[:])
	out = secretbox.Seal(out, content, nonce, &key)

	return out, nil
}
func decryptFile(key [common.KeySize]byte, encryptedContent []byte) ([]byte, error) {

	if len(encryptedContent) < (common.NonceSize + secretbox.Overhead) {
		return nil, common.FileDecryptionError
	}
	var nonce [common.NonceSize]byte
	copy(nonce[:], encryptedContent[:common.NonceSize])
	out, ok := secretbox.Open(nil, encryptedContent[common.NonceSize:], &nonce, &key)
	if !ok {
		return nil, common.FileDecryptionError
	}
	return out, nil
}


func generateNonce() (*[common.NonceSize]byte, error) {
	nonce := new([common.NonceSize]byte)
	_, err := rand.Reader.Read(nonce[:])
	if err != nil {
		return nil, err
	}
	return nonce, nil
}

func CheckFileExist(id string) (filename string, ok bool) {

	ok = false
	filename = "fake"
	session := common.Session.Copy()
	c := session.DB("firefly").C("file")
	if bson.IsObjectIdHex(id) {
		objectId := bson.ObjectIdHex(id)
		var file common.FireflyFile
		err := c.Find(bson.M{"_id": objectId}).One(&file)
		if err != nil {
			beego.Error(common.DBQueryError)
		}else {
			ok = true
			filenameInByte, _ := hex.DecodeString(file.FileName)
			filename = string(filenameInByte)
			return
		}
	}

	return
}
