package main
import (
	"io"
	"crypto/rand"
	"errors"
	"golang.org/x/crypto/nacl/secretbox"
	"fmt"
)

const (
	KeySize = 32
	NonceSize = 24
)

func main() {

	key, _ := GenerateKey()
	var message = []byte("this is a big secret")

	out, _ := Encrypt(key, message)
	fmt.Printf("%0x", out)
	msg, _ := Decrypt(key, out)
	fmt.Printf("%s", msg)
}

func GenerateKey() (*[KeySize]byte, error) {

	key := new([KeySize]byte)
	_, err := io.ReadFull(rand.Reader, key[:])
	if err != nil {
		return nil, err
	}
	return key, nil
}

func GenerateNonce() (*[NonceSize]byte, error) {

	nonce := new([NonceSize]byte)
	_, err := io.ReadFull(rand.Reader, nonce[:])
	if err != nil {
		return nil, err
	}
	return nonce, nil
}

var (
	EncryptErr = errors.New("secret: encryption failed")
	DecryptErr = errors.New("secret: decryption failed")
)

func Encrypt(key *[KeySize]byte, message []byte) ([]byte, error) {

	nonce, err := GenerateNonce()
	if err != nil {
		return nil, EncryptErr
	}
	out := make([]byte, len(nonce))
	copy(out, nonce[:])
	out = secretbox.Seal(out, message, nonce, key)
	return out, nil
}

func Decrypt(key *[KeySize]byte, message []byte) ([]byte, error) {

	if len(message) < (NonceSize + secretbox.Overhead) {
		return nil, DecryptErr
	}
	var nonce [NonceSize]byte
	copy(nonce[:], message[:NonceSize])
	out, ok := secretbox.Open(nil, message[NonceSize:], &nonce, key)
	if !ok {
		return nil, DecryptErr
	}
	return out, nil
}





