package routers

import (
	"firefly.com/firefly/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{})

	beego.Router("/login", &controllers.MemberController{}, "*:Login")
	beego.Router("/dologin", &controllers.MemberController{}, "post:DoLogin")
	beego.Router("/signup", &controllers.MemberController{}, "*:SignUp")
	beego.Router("/dosignup", &controllers.MemberController{}, "post:DoSignUp")

	beego.Router("/logout", &controllers.MemberController{}, "post:Logout")

	// check auth
	beego.Router("/upload", &controllers.MemberController{}, "*:Upload")
	beego.Router("/doupload", &controllers.MemberController{}, "post:DoUpload")

	// check auth
	beego.Router("/file", &controllers.MemberController{}, "*:File")
	// ([\w]+) doesn't work
	beego.Router("/p/:id:string", &controllers.FileController{}, "get:Preview")
	beego.Router("/dopreview", &controllers.FileController{}, "post:DoPreview")


	// token
	beego.Router("/download/:id:string/:token:string", &controllers.FileController{}, "get:Download")
	beego.Router("/bd/:id:string", &controllers.FileController{}, "get:BoxDownload")


	// humans.txt - ugly
	beego.SetStaticPath("/humans.txt", "static/humans.txt")

	// tests
//		beego.Router("/session", &controllers.MainController{}, "get:Session")
	//  beego.Router("/config", &controllers.MemberController{}, "get:Config")
//	beego.Router("/map", &controllers.FileController{}, "*:Map")
//	beego.Router("/drop", &controllers.FileController{}, "*:Drop")
//	beego.Router("/download", &controllers.MemberController{}, "*:Download")
	beego.Router("/updateboxview", &controllers.FileController{}, "*:UpdateBoxView")

	// box view demo
	//	beego.Router("/demo", &controllers.FileController{}, "get:Demo")
}
