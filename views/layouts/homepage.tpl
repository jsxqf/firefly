<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>firefly</title>
    <link rel="stylesheet" href="../../static/css/normalize.min.css">
    <link rel="stylesheet" href="../../static/css/foundation.min.css">
    <link rel="stylesheet" href="../../static/css/app.css">
    <!-- start Mixpanel -->
    <script type="text/javascript">(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f)}})(document,window.mixpanel||[]);
        mixpanel.init("9b585cb7c4ddedfb01742f955a15ef18");
    </script>
    <!-- end Mixpanel -->
</head>
<body>

<div class="contain-to-grid">
    <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
            <li class="name">
                <h1><a href="/">萤火虫</a></h1>
            </li>
            <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span>菜单</span></a></li>
        </ul>

        <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="right">
                <li class=""><a href="#features">特性</a></li>
                <li class=""><a href="#pricing">定价</a></li>
                <li class=""><a href="/login">登录</a></li>
                <li><a href="/signup" class="button">注册</a></li>
            </ul>
            <ul class="left">
                <li><a href="/upload" class="warning button">上传</a></li>
            </ul>
        </section>
    </nav>
</div>

<!--Orbit-->
<div class="row">
    <ul class="example-orbit" data-orbit>
        <li>
            <img src="//placehold.it/1000x400" alt="slide 1" />
            <div class="orbit-caption">
                <!--Caption One.-->
            </div>
        </li>
        <li class="active">
            <img src="//placehold.it/1000x400" alt="slide 2" />
            <div class="orbit-caption">
                <!--Caption Two.-->
            </div>
        </li>
        <li>
            <img src="//placehold.it/1000x400" alt="slide 3" />
            <div class="orbit-caption">
                <!--Caption Three.-->
            </div>
        </li>
    </ul>

</div>

<!--todo-->
<section id="features">
    <h3 class="text-center">为何选择 萤火虫 ？</h3>

    <!--todo-->
    <div class="row">
        <div class="small-12 medium-3 large-3 columns">
            <p>
                <img src="//twemoji.maxcdn.com/36x36/1f4cd.png" alt=""> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, aliquid aspernatur beatae debitis eos error incidunt, laboriosam natus nisi nostrum nulla obcaecati omnis provident quae quam rerum sed soluta, tempora.
            </p>
        </div>
        <div class="small-12 medium-3 large-3 columns">
            <p>
                <img src="//twemoji.maxcdn.com/36x36/1f60d.png" alt=""> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio, ex, voluptate? Aliquid, atque blanditiis, distinctio ea eaque fugit maiores maxime molestias, odio placeat porro quam qui rem repudiandae sequi similique.
            </p>
        </div>
        <div class="small-12 medium-3 large-3 columns">
            <p>
                <img src="//twemoji.maxcdn.com/36x36/1f680.png" alt=""> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio, ex, voluptate? Aliquid, atque blanditiis, distinctio ea eaque fugit maiores maxime molestias, odio placeat porro quam qui rem repudiandae sequi similique.
            </p>
        </div>
        <div class="small-12 medium-3 large-3 columns">
            <p>
                <img src="//twemoji.maxcdn.com/36x36/26f3.png" alt=""> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio, ex, voluptate? Aliquid, atque blanditiis, distinctio ea eaque fugit maiores maxime molestias, odio placeat porro quam qui rem repudiandae sequi similique.
            </p>
        </div>
    </div>
</section>
<br>
<section id="pricing">
    <br>
    <div class="row">
        <div class="medium-6 columns">
            <ul class="pricing-table">
                <li class="title">免费版</li>
                <li class="price">0 元/月</li>
                <li class="bullet-item">1<span> file upload per time</span></li>
                <li class="bullet-item">100<span> files total</span></li>
                <li class="bullet-item">1000<span> minutes expire</span></li>
                <li class="cta-button"><button class="tiny button" href="/signup">注册</button></li>
            </ul>
        </div>
        <div class="medium-6 columns">
            <ul class="pricing-table recommended">
                <li class="title">高级版</li>
                <li class="price">5 元/月</li>
                <li class="bullet-item">10<span> file upload per time</span></li>
                <li class="bullet-item">1000<span> files total</span></li>
                <li class="bullet-item">unlimited<span> minutes expire</span></li>
                <li class="cta-button"><button class="tiny alert button" href="#">即将推出</button></li>
            </ul>
        </div>
    </div>
</section>

<br>

<div class="footer-wrapper">
    <div class="row">
        <div class="small-10 columns text-center small-centered">
            <a href="/humans.txt"><span class='label'>&copy; billzhao & jsxqf</span></a>
        </div>
    </div>
</div>

<script type="text/javascript" src="../../static/js/jquery.min.js"></script>
<script type="text/javascript" src="../../static/js/foundation.min.js"></script>
<script type="text/javascript" src="../../static/js/foundation.orbit.js"></script>
<script type="text/javascript" src="../../static/js/app.js"></script>
<script>
    $(document).foundation();
    $(function () {

    });

    mixpanel.track("PV");
</script>
</body>
</html>