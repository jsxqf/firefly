<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>firefly</title>
    <link rel="stylesheet" href="../../static/css/normalize.min.css">
    <link rel="stylesheet" href="../../static/css/foundation.min.css">
    <link rel="stylesheet" href="../../static/css/foundation-icons.css">
    <!--todo min-->
    <link rel="stylesheet" href="../../static/css/dropzone.css">
    <link rel="stylesheet" href="../../static/css/sweetalert.css">
    <link rel="stylesheet" href="../../static/css/app.css">
</head>
<body>

<div class="contain-to-grid">
    <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
            <li class="name">
                <h1><a href="/">萤火虫</a></h1>
            </li>
            <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
            <li class="toggle-topbar menu-icon"><a href="#"><span>菜单</span></a></li>
        </ul>

        <section class="top-bar-section">
            <!-- Right Nav Section -->
            <ul class="right">
                <li class="has-dropdown">
                    <a href="#">{{.username}}</a>
                    <ul class="dropdown text-center">
                        <li><a href="/file">所有文件</a></li>
                        <li><a href="#" id="logout">登出</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="left">
                <li><a href="/upload" class="warning button">上传</a></li>
            </ul>
        </section>
    </nav>
</div>

<div class="content">
    {{.LayoutContent}}
</div>


<script type="text/javascript" src="../../static/js/jquery.min.js"></script>
<script type="text/javascript" src="../../static/js/foundation.min.js"></script>
<script type="text/javascript" src="../../static/js/foundation.topbar.js"></script>
<script type="text/javascript" src="../../static/js/app.js"></script>
<script>
    $(document).foundation();
    $(function () {

        $("#logout").on('click', function (evt) {
            evt.preventDefault();

            $.ajax({
                url: "/logout",
                type: 'POST',
                success: function (data) {
                    window.location.href = "/";
                },
                error: function (error) {
                    console.log(error);
                },
                complete: function () {
                }
            });

        });
    });
</script>
{{.UploadScript}}
{{.FileScript}}
</body>
</html>