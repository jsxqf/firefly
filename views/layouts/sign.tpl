<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>firefly</title>
    <link rel="stylesheet" href="../../static/css/normalize.min.css">
    <link rel="stylesheet" href="../../static/css/foundation.min.css">
    <link rel="stylesheet" href="../../static/css/foundation-icons.css">
    <link rel="stylesheet" href="../../static/css/app.css">
</head>
<body>

<div class="content">
    {{.LayoutContent}}
</div>


<script type="text/javascript" src="../../static/js/jquery.min.js"></script>
<script type="text/javascript" src="../../static/js/foundation.min.js"></script>
<script type="text/javascript" src="../../static/js/app.js"></script>
<script>
    $(document).foundation();
    $(function(){

    });
</script>
</body>
</html>