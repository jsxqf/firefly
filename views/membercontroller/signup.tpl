<div class="sign-panel">
    <div class="row">
        <div class="small-12 medium-8 large-8 columns small-centered medium-centered large-centered">
            <div class="panel">
                <div class="row">
                    <div class="small-10 medium-4 large-4 columns small-centered medium-centered large-centered">
                        <h3 class="text-center"><a href="/">萤火虫</a></h3>
                    </div>
                </div>

                <div class="small-10 columns small-centered">
                    <form action="/dosignup" method="post">
                        <div class="row collapse">
                            <div class="small-2 columns">
                                <span class="prefix"><i class="fi-torso"></i></span>
                            </div>
                            <div class="small-10 columns">
                                <input type="text" name="username" placeholder="用户名">
                            </div>
                        </div>
                        <div class="row collapse">
                            <div class="small-2 columns">
                                <span class="prefix"><i class="fi-mail"></i></span>
                            </div>
                            <div class="small-10 columns">
                                <input type="email" name="email" placeholder="邮箱">
                            </div>
                        </div>
                        <div class="row collapse">
                            <div class="small-2 columns">
                                <span class="prefix"><i class="fi-lock"></i></span>
                            </div>
                            <div class="small-10 columns">
                                <input type="password" name="password" placeholder="密码">
                            </div>
                        </div>
                        <div class="row collapse">
                            <button class="tiny button small-12 columns" type="submit">登录</button>
                        </div>

                        <div class="row collapse">
                            <div class="right">
                                <a href="/login" class="tiny button secondary">注册</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>