<script type="text/javascript" src="../../static/js/dropzone.min.js"></script>
<script type="text/javascript" src="../../static/js/sweetalert.min.js"></script>
<script type="text/javascript" src="//webapi.amap.com/maps?v=1.3&key=65c2a7defa5c26874b8bafc3f5035126"></script>
<script>

    Dropzone.autoDiscover = false;
    var fileNum = 0;

    $(function () {

        var lang = "zh",
                dropzoneMessage = "Drop files here to upload", swalTitle = "Location Is Missing !", swalText = "Don't use location ?", swalCancelButtonText = "Cancel", swalConfirmButtonText = "YES";
        if (lang == "zh") {
            dropzoneMessage = "将文件拖拽至此处上传";
            swalTitle = "地理位置为空！";
            swalText = "不使用地理位置？";
            swalCancelButtonText = "取消";
            swalConfirmButtonText = "确定";
        }

        fireflyDropzone = new Dropzone("#fireflyDropzone", {
            maxFilesize: 20,
            // remove remove
            addRemoveLinks: true,
            autoProcessQueue: false,
            maxFiles: 1,
            dictDefaultMessage: dropzoneMessage
        });
        fireflyDropzone.on("addedfile", function (file) {
            fileNum++;
            file.previewElement.addEventListener("click", function () {
                fireflyDropzone.removeFile(file);
                fileNum--;
            });
        });
        fireflyDropzone.on("success", function (response) {
            if (response.xhr.status == 200) {
//                window.location.href = "/file";
            } else {
                console.log("upload fail");
            }
        });


        var position = {};
        if (navigator.geolocation) {

            mapObj = new AMap.Map('map');
            mapObj.plugin('AMap.Geolocation', function () {
                geolocation = new AMap.Geolocation({
                    enableHighAccuracy: true,//是否使用高精度定位，默认:true
                    timeout: 8000,          //超过10秒后停止定位，默认：无穷大
                    maximumAge: 0,           //定位结果缓存0毫秒，默认：0
                    convert: true,           //自动偏移坐标，偏移后的坐标为高德坐标，默认：true
                    showButton: true,        //显示定位按钮，默认：true
                    buttonPosition: 'LB',    //定位按钮停靠位置，默认：'LB'，左下角
                    buttonOffset: new AMap.Pixel(10, 20),//定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
                    showMarker: true,        //定位成功后在定位到的位置显示点标记，默认：true
                    showCircle: true,        //定位成功后用圆圈表示定位精度范围，默认：true
                    panToLocation: true,     //定位成功后将定位到的位置作为地图中心点，默认：true
                    zoomToAccuracy: true      //定位成功后调整地图视野范围使定位位置及精度范围视野内可见，默认：false
                });
                mapObj.addControl(geolocation);
                AMap.event.addListener(geolocation, 'complete', onComplete);//返回定位信息
                AMap.event.addListener(geolocation, 'error', onError);      //返回定位出错信息
            });


            var marker = new AMap.Marker({
                position: mapObj.getCenter(),
                draggable: true, //点标记可拖拽
                cursor: 'move',  //鼠标悬停点标记时的鼠标样式
                raiseOnDrag: true//鼠标拖拽点标记时开启点标记离开地图的效果
            });
            marker.setMap(mapObj);
            AMap.event.addListener(marker, "dragend", onDragend);
            function onDragend() {
                var pos = marker.getPosition();

                position.ok = true;
                position.type = "custom";
                position.longitude = pos.getLng();
                position.latitude = pos.getLat();
                console.log(position);
            }

            mapObj.plugin(["AMap.ToolBar"], function () {
                //加载工具条
                var tool = new AMap.ToolBar();
                mapObj.addControl(tool);
            });


//            mapObj.plugin(["AMap.MouseTool"],function(){
//                var mousetool = new AMap.MouseTool(mapObj);
//                var lock = false;
//                mousetool.marker({
//                    content: "",
//                    draggable: true
//                }); //使用鼠标工具，在地图上画标记点
//
//            });
        } else {
            console.log("geolocation IS NOT available");
        }

        function onComplete(GeolocationResult) {
            position.ok = true;
            position.type = "fuzzy";
            position.longitude = GeolocationResult.position.lng;
            position.latitude = GeolocationResult.position.lat;
            if (GeolocationResult.accuracy != null) {
                position.type = "accurate";
            }
            console.log(GeolocationResult);
            console.log(position);
        }

        function onError(GeolocationError) {
            position.ok = false;
            console.log(position);
        }

        // #upload
        $('#upload').on('click', function (evt) {
            evt.preventDefault();

            var expireTime = $('#expire-time').val();
            if (expireTime != '0') {
                $('#expire').val(expireTime);
            }
            if (!position.ok) {
                swal({
                    title: swalTitle,
                    text: swalText,
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: swalCancelButtonText,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: swalConfirmButtonText,
                    closeOnConfirm: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        //
                        console.log("sweetalert confirm");
                        $('#position').val(JSON.stringify(position));
                        if (fileNum >= 1) {
                            fireflyDropzone.processQueue();
                        }
                    } else {
                        return false;
                    }
                });
            } else {
                console.log(JSON.stringify(position));
                $('#position').val(JSON.stringify(position));
                if (fileNum >= 1) {
                    fireflyDropzone.processQueue();
                }
                console.log("get location");
            }
        });

    })
</script>