<div class="sign-panel">
    <div class="row">
        <div class="small-12 medium-8 large-8 columns small-centered medium-centered large-centered">
            <div class="panel">
                <div class="row">
                    <div class="small-12 medium-4 large-4 columns small-centered medium-centered large-centered">
                        <h3 class="text-center"><a href="/">萤火虫</a></h3>
                    </div>
                </div>

                <div class="small-12 columns small-centered">
                    <form action="/dologin" method="post">
                        <div class="row collapse">
                            <div class="small-2 columns">
                                <span class="prefix"><i class="fi-mail"></i></span>
                            </div>
                            <div class="small-10 columns">
                                <input type="email" name="email" placeholder="邮箱">
                            </div>
                        </div>
                        <div class="row collapse">
                            <div class="small-2 columns">
                                <span class="prefix"><i class="fi-lock"></i></span>
                            </div>
                            <div class="small-10 columns">
                                <input type="password" name="password" placeholder="密码">
                            </div>
                        </div>
                        <div class="row collapse">
                            <button class="button tiny small-12 columns">登录</button>
                        </div>

                        <div class="row collapse">
                            <div class="button-bar">
                                <ul class="button-group round even-2 small-12 columns small-centered">
                                    <li><a href="/forgetPass" class="tiny button secondary">忘记密码</a></li>
                                    <li><a href="/signup" class="tiny button secondary">登录</a></li>
                                </ul>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>

    </div>
</div>