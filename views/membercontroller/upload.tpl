<br>

<div class="row">
    <div class="small-12 columns small-centered">
        <form action="/doupload" id="fireflyDropzone" class="dropzone">
            <input type="hidden" name="_csrfToken" value="{{._csrfToken}}">
            <input type="hidden" name="position" value="" id="position">
            <input type="hidden" name="expire" value="3600" id="expire">
        </form>
    </div>
    <br>
    <div class="small-4 columns left">
        <div class="position-msg">
        </div>
    </div>
    <div class="small-8 medium-4 large-4 columns right">
        <div class="row collapse postfix-round">
            <div class="small-8 columns">
                <select name="expire-time" id="expire-time">
                    <option value="0">过期</option>
                    <option value="900">15 分钟</option>
                    <option value="3600">1 小时</option>
                    <option value="10800">3 小时</option>
                    <option value="43200">12 小时</option>
                </select>
            </div>
            <div class="small-4 columns">
                <a href="#" class="button postfix" id="upload">上传</a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="small-12 columns small-centered">
        <div id="map">

        </div>
    </div>
</div>

<br>

