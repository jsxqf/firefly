<br>

<!--<div class="row">
    <div class="small-12 medium-8 large-8 columns small-centered medium-centered large-centered">
        <input type="text" name="search" id="search">
    </div>
</div>-->

<div class="row">
    <table class="small-12 columns small-centered text-center">
        <thead>
        <tr>
            <th>文件名</th>
            <th>链接</th>
        </tr>
        </thead>
        <tbody>
        {{range $key, $value := .files}}
        <tr class="detail-table-row">
            <td>
                {{$value.FileName}}
                <br>
                <span class="info label">{{$value.CreateAt}} <span data-localize="{{$value.TimeType}}"></span></span>
                <span class="info label">{{$value.Position}}</span>
            </td>
            <td class="link">

                {{$.linkprefix}}{{$value.Id}}
                <a href="#" class="tiny success button round copy-button"
                   data-clipboard-text="{{$.domain}}{{$.linkprefix}}{{$value.Id}}">复制
                </a>
            </td>

        </tr>
        {{end}}

        </tbody>
    </table>
</div>