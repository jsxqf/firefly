<script type="text/javascript" src="//webapi.amap.com/maps?v=1.3&key=65c2a7defa5c26874b8bafc3f5035126"></script>
<script>
    $(function () {

        function initialize() {
            var position = new AMap.LngLat(116.397428, 39.90923);
            var mapObj = new AMap.Map("map", {
                view: new AMap.View2D({ //创建地图二维视口
                    center: position,//创建中心点坐标
                    zoom: 14, //设置地图缩放级别
                    rotation: 0 //设置地图旋转角度
                }),
                lang: "zh_cn"//设置地图语言类型，默认：中文简体
            });//创建地图实例
        }
    })
</script>