<!doctype html>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>map</title>
</head>
<body>

<div class="row">

    <div class="small-8 columns small-centered">
        <div class="container">
            <div id="map" style="width: 600px; height: 600px;">

            </div>
        </div>
    </div>

</div>

<script type="text/javascript" src="//webapi.amap.com/maps?v=1.3&key=65c2a7defa5c26874b8bafc3f5035126"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script>
    $(function () {
        initialize();

        function initialize() {
            var position = new AMap.LngLat(116.397428, 39.90923);
            var mapObj = new AMap.Map("map", {
                view: new AMap.View2D({ //创建地图二维视口
                    center: position,//创建中心点坐标
                    zoom: 16, //设置地图缩放级别
                    rotation: 0 //设置地图旋转角度
                }),
                lang: "zh_cn"//设置地图语言类型，默认：中文简体
            });//创建地图实例
        }
    })
</script>
</body>
</html>


