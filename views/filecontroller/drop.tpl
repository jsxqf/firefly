<!doctype html>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>drop</title>
    <link rel="stylesheet" href="../../static/css/basic.css">
    <link rel="stylesheet" href="../../static/css/dropzone.css">
</head>
<body>

<!--other data?-->
<form action="/doupload" class="dropzone" id="my-awesome-dropzone">
    <input type="hidden" name="_csrfToken" value="token">
</form>

<script type="text/javascript" src="../../static/js/dropzone.js"></script>
<script>
    Dropzone.options.myAwesomeDropzone = {
        maxFilesize: 2
//        paramName: "the_file"
    }
</script>
</body>
</html>