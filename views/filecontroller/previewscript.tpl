<script type="text/javascript" src="//webapi.amap.com/maps?v=1.3&key=65c2a7defa5c26874b8bafc3f5035126"></script>
<script>
    $(function () {
        var id = {{.id}};
        var position = JSON.parse({{.position}});
        var lang = getLang();
        var domain = {{.domain}};

        gettingLocation = "<span>定位中</span> ";
        gettingFilePreview = "<span>获取文件预览中</span> ";
        failToGetFilePreview = "<span>获取文件预览失败</span> ";
        errorOccur = "<span>出错了</span> ";

        if (!$.isEmptyObject(position)) {
            // check location
            var mapObj, geolocation;
            if (navigator.geolocation) {
                mapObj = new AMap.Map("map");
                mapObj.plugin("AMap.Geolocation", function () {
                    geolocation = new AMap.Geolocation({
                        enableHighAccuracy: true,
                        timeout: 8000,
                        maximumAge: 0,
                        convert: true,
                        showButton: false
                    });
                    mapObj.addControl(geolocation);
                    AMap.event.addListener(geolocation, 'complete', onComplete);
                    AMap.event.addListener(geolocation, 'error', onError);
                });
                geolocation.getCurrentPosition();
            }
        } else {
            // return preview
            getFilePreview(id, JSON.stringify({}));
        }

        function onComplete(GeolocationResult) {
            console.log(GeolocationResult.position);
            // bad highlight
            var pos = {};
            pos.longitude = GeolocationResult.position.lng;
            pos.latitude = GeolocationResult.position.lat;
            getFilePreview(id, JSON.stringify(pos));

        }

        function onError(GeolocationError) {
            console.log(GeolocationError);
            // loc fail
        }

        function getFilePreview(fileId, userPosition) {

            $(".msg").empty();
            $(".msg").append(gettingFilePreview);
            $.ajax({
                url: "/dopreview",
                type: 'POST',
                data: {id: fileId, position: userPosition},
                success: function (msg) {
                    // unexpected token o - already object
//                    var msg = JSON.parse(data);
                    console.log(msg);

                    $(".msg").empty();
                    if (msg.Ok) {
                        $("#box-view-session-url").attr("src", msg.Data);
                        // download link
                        $("#download").attr("href", domain + "download/" + id + "/" + msg.Extra);
                    } else {
                        $(".msg").append(errorOccur);
                    }
                },
                error: function (error) {
                    $(".msg").empty();
                    $(".msg").append(errorOccur);
                    console.log(error);
                },
                complete: function () {
                    console.log('complete');
                }
            });
        }
    });
</script>