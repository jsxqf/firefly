package controllers
import "github.com/astaxie/beego"

type ErrorController struct {
	beego.Controller
}

func (this *ErrorController) Error404() {

	this.Data["content"] = "404"
	this.TplNames = "layouts/sign.tpl"
}

func (this *ErrorController) ErrorNoAuth(){

	this.TplNames = "layouts/sign.tpl"
}

func (this *ErrorController) ErrorExpire(){

	this.TplNames = "layouts/sign.tpl"
}