package controllers

import (
	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

func (this *MainController) Prepare(){

	this.Layout = "layouts/homepage.tpl"
}

func (this *MainController) Get() {

}

func (this *MainController) Session(){

	s := this.GetSession("username")
	this.Ctx.Output.Body([]byte(s.(string)))
}
