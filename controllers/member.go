package controllers
import (
	"github.com/astaxie/beego"
	"firefly.com/firefly/models/member"
	"net/http"
	"time"
	"firefly.com/firefly/models/fireflyfile"
	"io/ioutil"
	"gopkg.in/mgo.v2/bson"
	"strconv"
	"firefly.com/firefly/models/common"
	"encoding/hex"
	"encoding/json"
)

type MemberController struct {
	beego.Controller
}

func (this *MemberController) Login() {

	this.Layout = "layouts/sign.tpl"
}

func (this *MemberController) DoLogin() {

	email := this.GetString("email")
	password := this.GetString("password")

	if len(email) >= 5 && len(password) >= 5 {

		ok, ret := member.DoLogin(email, password)
		if ok {
			// set session
			this.SetSession("username", ret.UserName)
			this.SetSession("email", ret.Email)
			this.Redirect("/file", http.StatusTemporaryRedirect)
		}else {
			this.Redirect("/login", http.StatusTemporaryRedirect)
		}
	}else {
		this.Redirect("/login", http.StatusTemporaryRedirect)
	}
}

func (this *MemberController) SignUp() {

	this.Layout = "layouts/sign.tpl"
}

func (this *MemberController) DoSignUp() {

	beego.Notice("receive post request")

	username := this.GetString("username")
	email := this.GetString("email")
	password := this.GetString("password")
	createAt := time.Now().Unix()

	m := &common.Member{username, email, password, createAt}
	ok := member.DoSignUp(m)
	if ok {
		this.Redirect("/login", http.StatusTemporaryRedirect)
	}else {
		this.Ctx.Output.Body([]byte("Sign Up Fail :("))
	}
}

func (this *MemberController) Upload() {

	if this.checkAuth() {
		this.Layout = "layouts/member.tpl"
		timestamp := time.Now().UnixNano()
		this.SetSession("_csrfToken", timestamp)
		this.Data["_csrfToken"] = timestamp
		this.Data["username"] = this.GetSession("username")
		this.LayoutSections = make(map[string]string)
		this.LayoutSections["UploadScript"] = "membercontroller/uploadscript.tpl"
	}else {
		// todo
		this.Redirect("/login", http.StatusTemporaryRedirect)
	}
}
func (this *MemberController) DoUpload() {

	if this.checkAuth() {
		file, fileHeader, err := this.GetFile("file")
		tokenPassed, _ := this.GetInt64("_csrfToken")
		position := this.GetString("position")
		expire, _ := this.GetInt64("expire")

		if err != nil {
			beego.Error(common.FileGetFromFormError)
			this.Ctx.Output.Body([]byte("fail"))
		}else if this.checkCSRFToken(tokenPassed) || (expire != 900 && expire != 3600 && expire != 10800 && expire != 43200) {
			// todo file size check?
			// http.MaxBytesReader
			content, _ := ioutil.ReadAll(file)
			if (int64(len(content)) < 1024*1024*20) {
				var fireflyFile common.FireflyFile
				fireflyFile.Id = bson.NewObjectId()
				// type assertion
				fireflyFile.Owner = (this.GetSession("email")).(string)
				fireflyFile.FileName = hex.EncodeToString([]byte(fileHeader.Filename))
				fireflyFile.Position = position
				fireflyFile.Expire = expire
				timestamp := time.Now().Unix()
				fireflyFile.CreateAt = timestamp
				fireflyFile.UpdateAt = timestamp

				this.SaveToFile("file", common.JsonConfig.String("FILE_FOLDER")+common.JsonConfig.String("TMP_FILE_FOLDER")+fireflyFile.Id.Hex())

				ok := fireflyfile.SaveFileToDB(fireflyFile, content)

				if ok {
					// generate box view session
//					 un in prd
					go func(){
						boxViewSession := fireflyfile.GenerateBoxViewSession(fireflyFile.Id.Hex())
						fireflyfile.UpdateBoxView(fireflyFile.Id, boxViewSession)
					}()

					this.Ctx.Output.Body([]byte("save file success"))
				}else {
					beego.Error(common.FileSaveToDBError)
				}
				this.Ctx.Output.Body([]byte("doupload"))
			}else {
				this.Ctx.Output.Body([]byte("size too large."))
			}

		}else {
			// todo return error
			this.Ctx.Output.Body([]byte("token || expire fail."))
			//			this.Redirect("/login", http.StatusTemporaryRedirect)
		}
	}else {
		this.Ctx.Output.Body([]byte("check auth fail."))
		//		this.Redirect("/login", http.StatusTemporaryRedirect)
	}

}

func (this *MemberController) Logout() {

	this.DestroySession()
	this.Ctx.Output.Body([]byte("logout"))
}

func (this *MemberController) File() {

	if this.checkAuth() {
		this.Layout = "layouts/member.tpl"
		this.Data["username"] = this.GetSession("username")
		owner := this.GetSession("email").(string)
		this.Data["domain"] = common.JsonConfig.String("DOMAIN")
		this.Data["linkprefix"] = common.JsonConfig.String("LINK_PREFIX")
		files := member.GetFile(owner)
		type fakeFile struct {
			Id       string
			Owner    string
			FileName string
			Position string
			Expire   int64
			// valid;todo_removed;
			CreateAt string
			UpdateAt int64
			TimeType string
		}
		var fakeFiles []fakeFile
		now := time.Now().Unix()
		for _, v := range files {
			var filePosition common.Position
			var f fakeFile
			f.Id = v.Id.Hex()
			f.Owner = v.Owner
			fileNameInBytes, _ := hex.DecodeString(v.FileName)
			f.FileName = string(fileNameInBytes)
			json.Unmarshal([]byte(v.Position), &filePosition)
			if filePosition == (common.Position{}) {
				f.Position = "0, 0"
			}else{
				f.Position = strconv.FormatFloat(filePosition.Longitude, 'f', 6, 64)+", "+strconv.FormatFloat(filePosition.Latitude, 'f', 6, 64)
			}
			f.Expire = v.Expire
			// human time
			timeGap := now - v.CreateAt
			if day := (timeGap /  (60*60*24)); day >= 1 {
				f.CreateAt = strconv.FormatInt(day, 10)
				f.TimeType = "DAYS_AGO"
			}else if hour := (timeGap / (60*60)); hour >=1 {
				f.CreateAt = strconv.FormatInt(hour, 10)
				f.TimeType = "HOURS_AGO"
			}else if minute := (timeGap / (60)); minute >=1 {
				f.CreateAt = strconv.FormatInt(minute, 10)
				f.TimeType = "MINS_AGO"
			}else {
				f.CreateAt = ""
				f.TimeType = "JUST_NOW"
			}
			f.UpdateAt = v.UpdateAt

			fakeFiles = append(fakeFiles, f)
		}
		this.Data["files"] = fakeFiles
		this.LayoutSections = make(map[string]string)
		this.LayoutSections["FileScript"] = "membercontroller/filescript.tpl"
	}else {
		this.Redirect("/login", http.StatusTemporaryRedirect)
	}
}

func (this *MemberController) checkAuth() (ok bool) {

	ok = false
	username := this.GetSession("username")
	if username != nil {
		ok = true
	}

	return
}
func (this *MemberController) checkCSRFToken(tokenPassed int64) (ok bool) {

	ok = false
	csrfToken := (this.GetSession("_csrfToken")).(int64)
	if csrfToken == tokenPassed {
		ok = true
	}

	return
}
// check download valid - checksum


