package controllers
import (
	"github.com/astaxie/beego"
	"firefly.com/firefly/models/fireflyfile"
	"time"
	"encoding/json"
	"firefly.com/firefly/models/common"
	"math"
	"gopkg.in/mgo.v2/bson"
	"strconv"
)

type FileController struct {
	beego.Controller
}

func (this *FileController) Preview() {

	id := this.Ctx.Input.Param(":id")
	file := fireflyfile.GetFileById(id)
	if file != (common.FireflyFile{}) {
		now := time.Now().Unix()
		this.Data["expire"] = file.Expire
		if file.Expire > (now - file.CreateAt) {
			this.Data["id"] = file.Id.Hex()
			this.Data["position"] = file.Position
			this.Data["domain"] = common.JsonConfig.String("DOMAIN")
		}else {
			this.Abort("expire")
		}
	}else {
		this.Abort("404")
	}

	this.Layout = "layouts/link.tpl"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["PreviewScript"] = "filecontroller/previewscript.tpl"
}

func (this *FileController) DoPreview() {

	var msg common.Message
	msg.Ok = false
	msg.Message = "default"
	msg.Data = ""
	msg.Extra = ""

	type PositionFromClient struct {
		Longitude float64 `json:"longitude"`
		Latitude  float64 `json:"latitude"`
	}

	id := this.GetString("id")
	pos := this.GetString("position");
	var positionFromClient PositionFromClient
	json.Unmarshal([]byte(pos), &positionFromClient)

	var boxViewSession common.SessionRet

	file := fireflyfile.GetFileById(id)
	if file != (common.FireflyFile{}) {
		now := time.Now().Unix()
		if file.Expire > (now - file.CreateAt) {
			var positionOfFile common.Position
			json.Unmarshal([]byte(file.Position), &positionOfFile)
			if positionOfFile != (common.Position{}) {
				// not empty
				if inRange(positionOfFile, positionFromClient.Longitude, positionFromClient.Latitude) {
					msg.Ok = true
					msg.Message = "In Position"
					json.Unmarshal([]byte(file.BoxViewSession), &boxViewSession)
					// set download token
					timestamp := time.Now().UnixNano()
					this.SetSession("_downloadToken", timestamp)
					if boxViewSession != (common.SessionRet{}) {
						msg.Data = boxViewSession.Urls.View
					}
					msg.Extra = strconv.FormatInt(timestamp, 10)

				}else {
					msg.Message = "you are out of range"
				}
			}else {
				msg.Ok = true
				msg.Message = "Posion Not Required"
				json.Unmarshal([]byte(file.BoxViewSession), &boxViewSession)
				// set download token
				timestamp := time.Now().UnixNano()
				this.SetSession("_downloadToken", timestamp)

				if boxViewSession != (common.SessionRet{}) {
					msg.Data = boxViewSession.Urls.View
				}
				msg.Extra = strconv.FormatInt(timestamp, 10)

			}

		}else {
			msg.Message = "File Has Expired"
		}
	}

	this.Data["json"] = &msg
	this.ServeJson()

}

func inRange(pos common.Position, longitude, latitude float64) (ok bool) {
	ok = false
	if longitude == 0 || latitude == 0 {
		return
	}else {
		longitudeGap, _ := common.JsonConfig.Float("LONGITUDE_GAP")
		latitudeGap, _ := common.JsonConfig.Float("LATITUDE_GAP")
		if math.Abs(pos.Longitude-longitude) <= longitudeGap && math.Abs(pos.Latitude-latitude) <= latitudeGap {
			ok = true
			return
		}
	}
	return
}

// box view demo
//func (this *FileController) Demo() {
//
//	url := fireflyfile.RefreshBoxViewSession("fakeid")
//
//	this.Data["box_view_url"] = url
//	//	this.Ctx.Output.Body([]byte("demo"))
//}

// download file
func (this *FileController) Download() {

	id := this.Ctx.Input.Param(":id")
	tokenInString := this.Ctx.Input.Param(":token")

	// check id
	filename, ok := fireflyfile.CheckFileExist(id)
	tokenOk := this.checkDownloadToken(tokenInString)
	if ok && tokenOk {
		this.Ctx.Output.Download(common.JsonConfig.String("FILE_FOLDER")+common.JsonConfig.String("TMP_FILE_FOLDER")+id, filename)
	}else {
		this.Abort("404")
	}
}

func (this *FileController) BoxDownload() {

	// refer
	id := this.Ctx.Input.Param(":id")
	// check id
	if filename, ok := fireflyfile.CheckFileExist(id); ok {
		this.Ctx.Output.Download(common.JsonConfig.String("FILE_FOLDER")+common.JsonConfig.String("TMP_FILE_FOLDER")+id, filename)
	}else {
		this.Abort("404")
	}
}

func (this *FileController) UpdateBoxView() {

	id := bson.ObjectIdHex("55dc0604e75de333d7000001")
	ok := fireflyfile.UpdateBoxView(id, "updated")
	if ok {
		this.Ctx.Output.Body([]byte("update box success."))
	}else {
		this.Ctx.Output.Body([]byte("update box fail."))
	}
}

func (this *FileController) checkDownloadToken(token string) (ok bool) {

	ok = false
	if this.GetSession("_downloadToken") != nil {
		downloadToken := this.GetSession("_downloadToken").(int64)
		if token == strconv.FormatInt(downloadToken, 10) {
			ok = true
			return
		}
	}

	return
}

